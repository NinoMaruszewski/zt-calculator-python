#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Runs the calculator from the command line."""
import sys

from .app import run_cli, run_gui


if __name__ == "__main__":
    help_message = (
        'Type "gui" to run the gui calculator, '
        '"cli" to run the cli calculator, or "help" to display this message.'
    )
    if "gui" in sys.argv:
        run_gui()
    elif "cli" in sys.argv:
        run_cli()
    elif "help" in sys.argv:
        print(help_message)
    else:
        print("Invalid arguments.")
        print(help_message)
