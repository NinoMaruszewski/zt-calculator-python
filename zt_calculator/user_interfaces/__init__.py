#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# flake8: noqa
"""User interfaces for the ZT Calculator."""

from .cli import run as run_cli_interface
from .gui_tk import CalcFrame as TkCalcFrame
from .gui_wx import CalcFrame as WxCalcFrame
