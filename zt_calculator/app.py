#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Various functions for running the calculator."""
try:
    import wx
    wx_installed = True
except ModuleNotFoundError:
    import tkinter as tk
    wx_installed = False

if wx_installed:
    from .user_interfaces import WxCalcFrame
else:
    from .user_interfaces import TkCalcFrame
from .user_interfaces import run_cli_interface


def run_gui() -> None:
    """Run the GUI calculator."""
    if wx_installed:
        app = wx.app()
        calc = WxCalcFrame()
        calc.Show()
        app.MainLoop()
    else:
        root = tk.Tk()
        calc = TkCalcFrame(root)
        root.mainloop()


def run_cli() -> None:
    """Run the CLI UI calculator."""
    run_cli_interface()
