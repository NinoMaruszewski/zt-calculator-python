# Project Title

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [Contributing](../CONTRIBUTING.md)

## About

Calculator for the ZT of a thermoelectric material.

Rewrite and improvement of [this calculator](https://github.com/AlemSnyder/ZT-calculator).
## Getting Started

This will help you get this calculator up and running.

### Prerequisites

You should have `MatPlotLib` installed and optionally `WxPython` for a better UI. To install them, run:
```
pip install -r requirements.txt
```

## Usage

To run this calculator, just double click on `run.pyw` or type this in the terminal:
```
python -m zt_calculator gui
```
To run with a cli interface, run:
```
python -m zt_calculator cli
```
