#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Imports and runs the calculator with a gui."""
from zt_calculator import app

app.run_gui()
